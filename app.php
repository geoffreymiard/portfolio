<?php 

$cardRealise1 = [
    'img' => "/src/image/eurosport fantasy.jpg",
    'link' => "https://gitlab.com/geoffreymiard/eurosportfantasy",
    'title' => "Eurosport fantasy",
    'date' => "Janvier 2021",
    'techno' => "HTML / CSS",
    'description' => "Intégration en début de formation",
];
$cardRealise2 = [
    'img' => "src/image/SpaceX.jpg",
    'link' => "https://gitlab.com/geoffreymiard/spacex",
    'title' => "Blog SpaceX",
    'date' => "Février 2021",
    'techno' => "Bootstrap",
    'description' => "Réaliser une page web responsive",
];
$cardRealise3 = [
    'img' => "src/image/Citations.jpg",
    'link' => "https://gitlab.com/geoffreymiard/randomtext" ,
    'title' => "Générateur de citations",
    'date' => "Février 2021",
    'techno' => "JavaScript",
    'description' => "Utilisation des API en JavaScript",
];
$cardRealise4 = [
    'img' => "src/image/portfolio.jpg",
    'link' => "" ,
    'title' => "Portfolio",
    'date' => "février 2021 et mis à jour",
    'techno' => "HTML / CSS / JS /PHP",
    'description' => "Mise à jour régulière",
];
$comingSoon = [
    'img' => "src/image/upload2.png",
    'link' => "" ,
    'title' => "",
    'date' => "Coming soon",
    'techno' => "",
    'description' => "",
];

$competences1 = [
    'techno' => "HTML / CSS /BootStrap",
    'value' => "70%"
];
$competences2 = [
    'techno' => "JavaScript / jQuery",
    'value' => "45%"
];
$competences3 = [
    'techno' => "PHP",
    'value' => "40%"
];
$competences4 = [
    'techno' => "React",
    'value' => "30%"
];
$competences5 = [
    'techno' => "Photoshop / XD",
    'value' => "50%"
];
$competences6 = [
    'techno' => "GitLab",
    'value' => "60%"
];
$skills1 = [
    'soft' => "Management et formation",
    'value' => "80%"
];
$skills2 = [
    'soft' => "Esprit d'équipe",
    'value' => "80%"
];
$skills3 = [
    'soft' => "Methode agile",
    'value' => "60%"
];
$skills4 = [
    'soft' => "Rigueur / Discipline",
    'value' => "90%"
];
$skills5 = [
    'soft' => "Résistance au stress",
    'value' => "80%"
];
$skills6 = [
    'soft' => "Anglais",
    'value' => "85%"
];

$tableCompetences = [
    $competences1,
    $competences2,
    $competences3,
    $competences4,
    $competences5,
    $competences6,
];

$tableSkills = [
    $skills1,
    $skills2,
    $skills3,
    $skills4,
    $skills5,
    $skills6,
];

$tabRealise = [
    $cardRealise1,
    $cardRealise2,
    $cardRealise3,
    $cardRealise4,
    $comingSoon,
    $comingSoon,
];

?>


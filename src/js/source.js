// Variable

const themeButton = document.querySelectorAll(".theme");

// Switch theme

themeButton.forEach((button) => {
  button.addEventListener("click", (event) => {
    document.body.classList.remove("dark-theme", "light-theme");

    switch (event.target.id) {
      case "dark":
        document.body.classList.add("dark-theme");
        break;
      case "light":
        document.body.classList.add("light-theme");
        break;

      default:
        null;
    }
  });
});
